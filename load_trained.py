import numpy as np
import gym
from model import Model


def test(render, num_games):
    times = []
    sides = [0, 0]
    t = 0
    for i in range(num_games):
        state = env.reset()
        while True:
            if render:
                env.render()
            action = np.argmax(model.predict([state]), axis=1)[0]
            next_state, reward, done, info = env.step(action)
            sides[action] += 1

            state = next_state
            t += 1
            if done:
                break
        times.append(t)
        state = env.reset()
        t = 0
    #     print(sides)
    return times


checkpoint_number = 479

games_num = 100
displayed_games = 10
seed = 0
gym_env = 'CartPole-v1'
learn_rate = 1e-2
discount_factor = 0.8

model = Model(learn_rate, discount_factor, seed)
env = gym.make(gym_env)


model.load(checkpoint_number)

print(test(True, displayed_games))

env.close()
print("Done")
