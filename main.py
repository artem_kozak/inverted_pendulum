import numpy as np
import gym
from model import Model
from memory import Memory


def test(render, num_games):
    times = []
    sides = [0, 0]
    for i in range(num_games):
        state = env.reset()
        for j in range(max_steeps_in_game):
            if render:
                env.render()
            action = np.argmax(model.predict([state]), axis=1)[0]
            next_state, reward, done, info = env.step(action)
            sides[action] += 1

            state = next_state

            if done:
                break
        times.append(j)
        env.reset()
    return times


def get_action(state, epsilon):
    if np.random.random() < epsilon:
        return env.action_space.sample()
    else:
        return np.argmax(model.predict([state]), axis=1)[0]


def train():
    loss = 0
    prev_avg_time = 0
    epsilon = start_eps
    for i in range(games_num):
        state = env.reset()
        for j in range(max_steeps_in_game):
            action = get_action(state, epsilon)
            next_state, reward, done, _ = env.step(action)

            if done:
                if i > random_games_num and epsilon > min_eps:
                    epsilon *= delay_eps
                reward = loss_penalty

            memory.add(state, action, reward, next_state)
            state = next_state

            if done:
                break
        if i > random_games_num:
            loss += model.train(memory.get_batch(batch))
        if i > random_games_num and i % test_frequency == 0:
            times = test(False, test_games_num)
            avg_time = sum(times) / len(times)
            if avg_time > 200 and int(avg_time) > prev_avg_time:
                prev_avg_time = avg_time
                print(model.save(avg_time))
            print('Game:', i)
            print('Loss:', loss / test_frequency)
            print('Time:', avg_time)
            #             print('Epsilon', epsilon)
            print()
            loss = 0


games_num = 8000
test_games_num = 100
max_steeps_in_game = 500
random_games_num = 50
test_frequency = 200
loss_penalty = -100
seed = 0
memory = 10000000
batch = 256
gym_env = 'CartPole-v1'
learning_rate = 0.002
discount_factor = 0.8
start_eps = 1.0
min_eps = 0.5
delay_eps = 0.999

model = Model(learning_rate, discount_factor, seed)
memory = Memory(memory, batch, seed)
env = gym.make(gym_env)

train()
print(test(False, test_games_num))
env.close()

print("Done")
